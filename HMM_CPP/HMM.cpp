#include "HMM.h"

HMM::HMM(cv::Mat image)
{
  _width = image.cols;
  _height = image.rows;
  float alphaInitial = 1.0 / (_width * _height);
  
  alpha_prev = cv::Mat(_width, _height, CV_32FC1, cv::Scalar::all(alphaInitial));
  
}

cv::Mat HMM::processFrame(cv::Mat input)
{
  
  cv::Mat alpha(_width, _height, CV_32FC1, cv::Scalar::all(0));
  // Step 2a. Recursion:
  for (int i = 0; i < _width; i++) {
    for (int j = 0; j < _height; j++) {
      alpha.at<float>(i,j) = transitionSum(i, j);
      input.at<float>(i,j) += 1;
    }
  }

  for (int i = 0; i < _width; i++) {
    for (int j = 0; j < _height; j++) {
      alpha.at<float>(i,j) *= input.at<float>(i,j);
    }
  }

  // Step 2b Normalisation:
  float N = 1.0/(cv::sum(alpha)[0]);
  alpha = alpha * N;
      
  
}

float HMM::transitionSum(int i, int j)
{
  float sum = 0; // Initalise the four results to 0s
  
  int xFactor, yFactor;
  float probFactor = 1.0/25.0;

  for(int x = -2; x < 3; x++){
    int Col = i + x;
    if(Col < 0){
      Col += _width - 1;
    }
    if(Col >= _width){
      Col %= _width;
    }
    for(int y = -2; y < 3; y++){
      int Row =  j + y;
      if(Row < 0){
        Row += _height - 1;
      }
      if(Row >= _height){
        Row %= _height;
      }

      sum += alpha_prev.at<float>(Row,Col);
    }
  }

  sum *= probFactor;
  return sum;
}