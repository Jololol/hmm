#ifndef HMM_H
#define HMM_H

#include <iostream>
#include "opencv2/core.hpp"

class HMM
{

public:
	HMM(cv::Mat image);

	cv::Mat processFrame(cv::Mat input);
private:
	float transitionSum(int i, int j);

	int _width, _height;

	float Beta = 0.75;

	cv::Mat alpha; // The main alpha updated after each process
	
  cv::Mat alpha_prev;  // Remembers the previous alpha values for each branch


};

#endif // HMM_H