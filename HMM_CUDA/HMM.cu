#include <iostream>
#include <limits.h>
#include <cstdlib>
#include <assert.h>

#include "opencv2/opencv.hpp"
#include "opencv2/core.hpp"

#include "opencv2/highgui.hpp"

#include "opencv2/cudaarithm.hpp"
#include <chrono>

using namespace std;

void printCSV(cv::Mat input);
void testNumbers(int r); // Simple test using number based array
void testImage(int r); // Simple test using images

void testSequence();

__global__ void multiplyGpuMat(cv::cuda::PtrStepSzf left, cv::cuda::PtrStepSzf right);


cv::Mat processFrame(cv::Mat &input, cv::cuda::GpuMat& alpha_prev);

#define IMAGE_SIZE 5


__device__ int getGlobalIdx_2D_2D()
{
	int blockId = blockIdx.x + blockIdx.y * gridDim.x;
	int threadId = blockId * (blockDim.x * blockDim.y) + (threadIdx.y * blockDim.x) + threadIdx.x;
	return threadId;
}

__global__ void fillMat(cv::cuda::PtrStepSzf input)
{
	int size = input.rows * input.cols;
	int globalIdx = getGlobalIdx_2D_2D();

	for (int i = globalIdx; i < size; i += blockDim.y * gridDim.y * blockDim.x * gridDim.x) {
		int x = i % input.cols;
		int y = i / input.rows;
		if (x <= input.cols - 1 && y <= input.rows - 1 && y >= 0 && x >= 0) {
			input(y, x) = ((y * input.cols) + x) / (float)size;
		}

	}

}


__global__ void HMMTransition(cv::cuda::PtrStepSzf input, cv::cuda::PtrStepSzf output, int patch_size)
{
	// Variables for indexing the GpuMat (Passed as PtrStepSz)

	int imgSize = input.rows * input.cols;
	int globalIdx = getGlobalIdx_2D_2D();
	int Row, Col; // Variables to specify Row and Column access
	int width = input.cols;
	int height = input.rows;
	
	float probFactor = 1.0/(patch_size * patch_size);
	
	float sum = 0;


	for (int idx = globalIdx; idx < imgSize; idx += blockDim.y * gridDim.y * blockDim.x * gridDim.x) {
		int x = idx % input.cols;
		int y = idx / input.cols;

		for (int i = -2; i < 3; i++) {
			Row = y + i; // Row in GpuMat to access
			
			if(Row < 0){
				Row += height - 1; // If row < 0, wrap around to bottom of image
			}
			if(Row > height - 1){
				Row = Row % height; // If row > height, wrap around to top of image
			}
			
			for (int j = -2; j < 3; j++) {

				Col = x + j; // Column in GpuMat to access

				if(Col < 0){ // If Col < 0, wrap around to right side of image
					Col += width - 1;
				}
				if(Col > width - 1){ // If Col > width, wrap around to left side of image
					Col = Col % width; 
				}
				
				sum += probFactor * input(Row, Col);

			}
		}

		output(y, x) = sum;

	}

}

int main(int argc, char** argv)
{
	
	//testNumbers(r);
	//testImage(r);
	//testMultiply();

	testSequence();


}



void testSequence()
{	
	int n = 100;
	int height = n;
	int width = n;
	float alphaInitial = 1.0/(n*n);
	cv::Mat image(width, height, CV_32FC1, cv::Scalar::all(alphaInitial));
	cv::cuda::GpuMat alpha_prev;
	alpha_prev.upload(image);
	
	image = image * 0.0;

	
	//cv::namedWindow("testing", cv::WINDOW_NORMAL);

	for (int t = 0; t < n; t++) {
		int x = t;
		int y = t;

		//cout << "Test number : " << t + 1 << endl;
		image.at<float>(y, x) = 255.0;
		imshow("input", image);

		// Block of  processes frames and times it.
		cv::Mat output = processFrame(image, alpha_prev);
		
		cv::Mat display = output;
		imshow("output", display);
		cv::waitKey(50);
		image.at<float>(y, x) = 0;
		
	}


}

/*

void testNumbers(int r)
{

	cv::Mat input(IMAGE_SIZE, IMAGE_SIZE, CV_32FC1);

	std::cout << "Got here!" << std::endl;

	cv::cuda::GpuMat src;
	src.upload(input);

	dim3 threadsPerBlock(32, 32);
	dim3 numBlocks(
	  static_cast<int>(std::ceil(input.size().width /
	                             static_cast<double>(threadsPerBlock.x))),
	  static_cast<int>(std::ceil(input.size().height /
	                             static_cast<double>(threadsPerBlock.y))));

	std::cout << "Accessing kernel with following threads/blocks." << std::endl;
	std::cout << "Threads(x,y): (" << threadsPerBlock.x << "," << threadsPerBlock.y << ")" << std::endl;
	std::cout << "Blocks(x,y): (" << numBlocks.x << "," << numBlocks.y << ")" << std::endl;
	fillMat <<< numBlocks, threadsPerBlock>>>(src);
	cudaDeviceSynchronize();


	src.download(input);
	cout << "Original matrix input: " << endl;
	cout << input << endl << endl;

	imshow("Output", input);
	cv::waitKey(0);

	cv::cuda::GpuMat dst(src.rows, src.cols, src.type());

	auto start = std::chrono::high_resolution_clock::now();

	// Perform maxTrans
	maxTrans <<< numBlocks, threadsPerBlock>>>(src, dst, r);
	cudaDeviceSynchronize();

	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> diff = end - start;

	printf("The duration is %f second.\n", diff.count());


	dst.download(input);
	cout << input << endl << endl;

	imshow("Output", input);
	cv::waitKey(0);
}*/

/*
void testImage(int r)
{


	//Load in test image
	cv::Mat input = cv::imread("test.png", 2);

	// Conver to floating point
	input.convertTo(input, CV_32FC1, (1.0 / 255.0));
	imshow("input after conversion to 32F", input);
	cv::waitKey(0);


	if (input.empty()) {
		cout << "Could not open or find image" << endl;
		return;
	}


	cv::cuda::GpuMat src;
	src.upload(input);

	dim3 threadsPerBlock(8, 8);
	std::cout << input.size().width << std::endl;
	std::cout << input.size().height << std::endl;

	dim3 numBlocks(
	  static_cast<int>(std::ceil(input.size().width /
	                             static_cast<double>(threadsPerBlock.x))),
	  static_cast<int>(std::ceil(input.size().height /
	                             static_cast<double>(threadsPerBlock.y))));


	std::cout << "Accessing kernel with following threads/blocks." << std::endl;
	std::cout << "Threads(x,y): (" << threadsPerBlock.x << "," << threadsPerBlock.y << ")" << std::endl;
	std::cout << "Blocks(x,y): (" << numBlocks.x << "," << numBlocks.y << ")" << std::endl;

	std::cout << "Image size is: " << input.rows * input.cols << std::endl;
	std::cout << "Threads size is: " << threadsPerBlock.x * threadsPerBlock.y * numBlocks.x * numBlocks.y << std::endl;

	src.download(input);
	imshow("input", input);

	cv::cuda::GpuMat dst(src.rows, src.cols, src.type());

	auto start = std::chrono::high_resolution_clock::now();

	maxTrans <<< numBlocks, threadsPerBlock>>>(src, dst, r);

	cudaDeviceSynchronize();

	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> diff = end - start;

	printf("The duration is %f second.\n", diff.count());


	dst.download(input);

	imshow("Output", input);
	cv::waitKey(0);
}*/

cv::Mat processFrame(cv::Mat &input, cv::cuda::GpuMat& alpha_prev)
{
	float Beta = 0.9;
	int patch_size = 5;

	// Variables for calling CUDA Kernel
	dim3 threadsPerBlock(8, 8);
	dim3 numBlocks(
	  static_cast<int>(std::ceil(input.size().width /
	                             static_cast<double>(threadsPerBlock.x))),
	  static_cast<int>(std::ceil(input.size().height /
	                             static_cast<double>(threadsPerBlock.y))));
	cv::Mat test;

	cv::cuda::GpuMat alpha;
	alpha.upload(input);
	// Add +1 to output from Morphology
	alpha.convertTo(alpha, alpha.type(), 1.0, 1.0);


	cv::cuda::GpuMat dst(input.rows, input.cols, input.type()); // Temporary dst for maxTrans output

	// Finding maxTrans for specific r
	HMMTransition <<< numBlocks, threadsPerBlock>>>(alpha_prev, dst, patch_size);
	cudaDeviceSynchronize(); // Wait for device to complete previous kernel call


	// Multiply results from HMMTransition

	multiplyGpuMat <<< numBlocks, threadsPerBlock>>>(alpha, dst);
	cudaDeviceSynchronize();

	


	// Get normalization factor
	float N = 1.0/(cv::cuda::sum(alpha)[0]);

	// Multiply N by normalization factor 
	alpha.convertTo(alpha, alpha.type(), N);
	cudaDeviceSynchronize();



	alpha_prev = alpha.clone();
	
	cv::Mat output;

	alpha.download(output);

	return output;
}

__global__ void multiplyGpuMat(cv::cuda::PtrStepSzf left, cv::cuda::PtrStepSzf right)
{
	int size = left.rows * left.cols;
	int globalIdx = getGlobalIdx_2D_2D();

	for (int i = globalIdx; i < size; i += blockDim.y * gridDim.y * blockDim.x * gridDim.x) {
		int x = i % left.cols;
		int y = i / left.cols;
		if (x <= left.cols - 1 && y <= left.rows - 1 && y >= 0 && x >= 0) {
			left(y, x) = left(y, x) * right(y, x);
		}

	}

}


void printCSV(cv::Mat input)
{
	std::ofstream CSV_Output;
	CSV_Output.open("Cuda_Output.csv", std::ofstream::app);;
	CSV_Output << cv::format(input, 2);
	CSV_Output.close();
}




